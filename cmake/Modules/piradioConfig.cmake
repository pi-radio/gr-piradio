INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_PIRADIO piradio)

FIND_PATH(
    PIRADIO_INCLUDE_DIRS
    NAMES piradio/api.h
    HINTS $ENV{PIRADIO_DIR}/include
        ${PC_PIRADIO_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    PIRADIO_LIBRARIES
    NAMES gnuradio-piradio
    HINTS $ENV{PIRADIO_DIR}/lib
        ${PC_PIRADIO_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/piradioTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PIRADIO DEFAULT_MSG PIRADIO_LIBRARIES PIRADIO_INCLUDE_DIRS)
MARK_AS_ADVANCED(PIRADIO_LIBRARIES PIRADIO_INCLUDE_DIRS)
