/* -*- c++ -*- */
/*
 * Copyright 2020 gr-piradio author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_PIRADIO_PIRADIO_SINK_H
#define INCLUDED_PIRADIO_PIRADIO_SINK_H

#include <piradio/api.h>
#include <gnuradio/hier_block2.h>

namespace gr {
namespace piradio {

/*!
 * \brief <+description of block+>
 * \ingroup piradio
 *
 */
class PIRADIO_API piradio_sink : virtual public gr::hier_block2 {
public:
  typedef boost::shared_ptr<piradio_sink> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of piradio::piradio_sink.
   *
   * To avoid accidental use of raw pointers, piradio::piradio_sink's
   * constructor is in a private implementation
   * class. piradio::piradio_sink::make is the public interface for
   * creating new instances.
   */
  static sptr make(size_t nstreams, const std::string &remote_host,
                   const std::vector<int> &stream_ports, int ctrl_port, uint8_t led_pattern);
};

} // namespace piradio
} // namespace gr

#endif /* INCLUDED_PIRADIO_PIRADIO_SINK_H */

