/* -*- c++ -*- */

#define PIRADIO_API

%include "gnuradio.i"           // the common stuff

//load generated python docstrings
%include "piradio_swig_doc.i"

%{
#include "piradio/piradio_sink.h"
%}

%include "piradio/piradio_sink.h"
GR_SWIG_BLOCK_MAGIC2(piradio, piradio_sink);
