/* -*- c++ -*- */
/*
 * Copyright 2020 gr-piradio author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <string>
#include "piradio_sink_impl.h"

namespace gr {
namespace piradio {

piradio_sink::sptr piradio_sink::make(size_t nstreams,
                                      const std::string &remote_host,
                                      const std::vector<int> &stream_ports, int ctrl_port,
                                      uint8_t led_pattern)
{
  return gnuradio::get_initial_sptr(
           new piradio_sink_impl(nstreams, remote_host, stream_ports, ctrl_port,
                                 led_pattern));
}

/*
 * The private constructor
 */
piradio_sink_impl::piradio_sink_impl(size_t nstreams,
                                     const std::string &remote_host,
                                     const std::vector<int> &stream_ports, int ctrl_port,
                                     uint8_t led_pattern) :
  gr::hier_block2("piradio_sink",
                  gr::io_signature::make(nstreams, nstreams,
                                         sizeof(short int)), gr::io_signature::make(0, 0, 0)), d_remote_host(
                                             remote_host), d_ctrl_port(ctrl_port), d_udp_stream_ports(
                                                 stream_ports)
{
  if (stream_ports.size() != nstreams) {
    throw std::invalid_argument("Number of streams must be equal to number of stream ports given");
  }
  if (stream_ports.size() <= 0) {
    throw std::invalid_argument("Must insert stream ports");
  }
  d_udp_blocks.resize(stream_ports.size());
  size_t c = 0;
  for (int p : d_udp_stream_ports) {
    d_udp_blocks[c] = gr::blocks::udp_sink::make(sizeof(short int),
                      remote_host, p, 1472, false);
    connect(self(), c, d_udp_blocks[c], 0);
    c++;
  }
  boost::asio::io_service io_service;
  boost::asio::ip::udp::resolver resolver(io_service);
  boost::asio::ip::udp::resolver::query query(remote_host,
      std::to_string(ctrl_port),
      boost::asio::ip::resolver_query_base::passive);
  d_endpoint = *resolver.resolve(query);
  d_socket = new boost::asio::ip::udp::socket(io_service);
  d_socket->open(d_endpoint.protocol());
  setup_radio(led_pattern);
}

/*
 * Our virtual destructor.
 */
piradio_sink_impl::~piradio_sink_impl()
{
  d_socket->close();
  delete d_socket;
}

void piradio_sink_impl::setup_radio(uint8_t mask)
{
  d_socket->send_to(boost::asio::buffer((void *) &mask, 1), d_endpoint);
}

} /* namespace piradio */
} /* namespace gr */

