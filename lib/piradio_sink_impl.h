/* -*- c++ -*- */
/*
 * Copyright 2020 gr-piradio author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_PIRADIO_PIRADIO_SINK_IMPL_H
#define INCLUDED_PIRADIO_PIRADIO_SINK_IMPL_H

#include <piradio/piradio_sink.h>
#include <gnuradio/blocks/udp_sink.h>
#include <boost/asio.hpp>
#include <vector>

namespace gr {
namespace piradio {

class piradio_sink_impl : public piradio_sink {
private:
  std::vector<gr::blocks::udp_sink::sptr> d_udp_blocks;
  std::vector<int>                        d_udp_stream_ports;
  int                                     d_ctrl_port;
  std::string                             d_remote_host;

  boost::asio::ip::udp::socket           *d_socket; // handle to socket
  boost::asio::ip::udp::endpoint          d_endpoint;

public:
  piradio_sink_impl(size_t nstreams, const std::string &remote_host,
                    const std::vector<int> &stream_ports, int ctrl_port, uint8_t led_pattern);
  ~piradio_sink_impl();

  void
  setup_radio(uint8_t mask);

  // Where all the action really happens
};

} // namespace piradio
} // namespace gr

#endif /* INCLUDED_PIRADIO_PIRADIO_SINK_IMPL_H */

